import axios from 'axios'
import qs from 'qs'

const loginChannelID = '';
const redirectURL = '';
const clientId ='';
const clientSecret ='';

export const GetLineAuthLink = () => {
  // code here
  return `https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=${loginChannelID}
    redirect_uri=${encodeURI(redirectURL)}&state=12345abcde&scope=profile%20openid&nonce=09876xyz`
  ;
}

export const GetLineAccessToken = async (code) => {
  // code here
  const body = qs.stringify({
		grant_type: "authorization_code",
		code,
		redirect_uri: redirectURL,
		client_id: clientId,
		client_secret: clientSecret,
	});

  try{
    return await axios.post("https://api.line.me/oauth2/v2.1/token",body);
  }catch (error){
    console.log(error);
  }
}

export const GetLineMeProfile = async (accessToken) => {
  // code here
  return await axios.get('https://api.line.me/v2/profile',
  {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    }
  })
    .catch( (error) => console.log(error))
}

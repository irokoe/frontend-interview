import axios from 'axios'

export const GetInoviceData = async ({ start, end }) => {
  // code here
  return await axios.get('http://localhost:5000/api/invoice',
  {
    params:{
      start,
      end,
    }
  })
    .then( (response) => {
      return response
    })
    .catch( (error) => console.log(error))
}

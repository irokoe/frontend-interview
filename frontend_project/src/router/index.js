import { createRouter, createWebHistory } from 'vue-router'
import { useStore } from '@/store'

const layoutRoutes = [
  {
    path: '/',
    name: 'overview',
    component: () => import('@/views/Overview/Overview.vue'),
    meta: {
      title: '發票總覽',
    },
  },
  {
    path: '/overview2',
    name: 'overview2',
    component: () => import('@/views/Overview/Overview.vue'),
    meta: {
      title: '發票異動',
    },
  },
]

const mainRoutes = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Login/Login.vue'),
    meta: {
      title: 'Login',
    },
  },
  {
    path: '/callback',
    name: 'callback',
    component: () => import('@/views/Callback/Callback.vue'),
    meta: {
      title: 'Callback',
    },
  },
  {
    path: '/',
    name: 'Layout',
    component: () => import('@/layout/index.vue'),
    meta: {
      requireAuth: true,
    },
    children: [...layoutRoutes],
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes: [...mainRoutes],
})

export default router

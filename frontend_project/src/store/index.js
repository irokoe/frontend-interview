import { defineStore } from 'pinia'

export const useStore = defineStore('app', {
  state: () => ({
    user: {},
  }),
  actions: {
    setUser (user) {
      this.user = user
    },
  },
})

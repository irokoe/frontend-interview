您好，首先很高興收到您的履歷，在面試前我們有個考題，請您評估是否實作，並且在執行完成後發MR到master

figma連結上附有手機版以及電腦版的畫面，主要功能為登入頁面以及每個年份發票消費統計

目前這個專案的框架是使用：
1. Native-UI
2. Windicss
3. Pinia

請您依照上面的框架執行該專案並完成以下功能：

1. 完成手機版切版，其中包含
    * 登入頁面
    * 後台內頁
    * 側邊欄須在寬未滿1200px時，將會自動收起來
2. 點擊漢堡選單時，側邊欄可以收合
3. 串接發票數據資料API
4. 匯出Excel
    * 匯出『所有年間的所有稅別』發票的數據
    * 欄位：
        * 年份
        * 税別
        * 數量
        * 銷售總額
5. LineLogin登入
6. 取得Line個人Profile名稱顯示在後台上
7. 修正頁碼無法切換的問題


### 前置步驟
1. 先git clone專案至本地
2. 至frontend_project資料夾、backend_project資料夾中安裝套件包
3. 至backend_project執行node index.js，backend將會啟動在本地上
4. 至frontend_project執行npm run dev，frontend將會啟動於本地上


### 參考資料
- [https://www.figma.com/file/fyBuwphnxER36RV9QswY1r/前端面試考題?node-id=0%3A1](https://www.figma.com/file/fyBuwphnxER36RV9QswY1r/%E5%89%8D%E7%AB%AF%E9%9D%A2%E8%A9%A6%E8%80%83%E9%A1%8C?node-id=0%3A1)
